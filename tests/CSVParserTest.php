<?php
/*
+--------------------------------------------------------------------+
| Copyright Rich Lott 2021. All rights reserved.                     |
|                                                                    |
| This work is published under the GNU GPLv3.0 license with some     |
| permitted exceptions and without any warranty. For full license    |
| and copyright information, see LICENSE                             |
+--------------------------------------------------------------------+
*/
use \ArtfulRobot\CSVParser;

class CSVParserTest extends \PHPUnit\Framework\TestCase {

  public function testBasicParse()
  {
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $this->basicParseAssertions($csv);
  }

  public function testBasicParseFromString()
  {
    $csv = file_get_contents(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv = CSVParser::createFromString($csv);
    $this->basicParseAssertions($csv);
  }
  public function testParseFileHeaderOnRow2()
  {
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-4.csv', NULL, 2);
    $this->basicParseAssertions($csv);
  }
  public function testParseStringHeaderOnRow2() {
    $csv = file_get_contents(dirname(__FILE__) . '/fixtures/testcase-4.csv');
    $csv = CSVParser::createFromString($csv, 2);
    $this->basicParseAssertions($csv);
  }
  public function testParseWithManualHeaders()
  {
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv', NULL, 0);

    // We expect 6 rows of data
    $this->assertEquals(6, $csv->count());
    $this->assertEquals('Name ', $csv->getCell(0));
    $this->assertEquals('  Age', $csv->getCell(1));

    // Name the cols
    $csv->setHeaders(['A', 'B']);
    $this->assertEquals('Name ', $csv->A);
    $this->assertEquals('  Age', $csv->B);
    $csv->setRow(2);
    $this->assertEquals('Rich', $csv->A);
    $this->assertEquals(40, $csv->B);

    // Set the first row as headers.
    $csv->extractHeaders(1);
    // This should mean we now have headers...
    $this->assertEquals('Name ', $csv->headers[0]);
    $this->assertEquals('  Age', $csv->headers[1]);
    // It should also 'rewind'
    $this->assertEquals(1, $csv->getRowNumber());
    // Which means the first row of data should now be...
    $this->assertEquals('Rich', $csv->Name);
    $this->assertEquals(40, $csv->Age);
  }
  protected function basicParseAssertions($csv) {
    // We should be at the start.
    $this->assertEquals(1, $csv->getRowNumber());
    $this->assertEquals(5, $csv->count());
    $i=0;
    $expectations = [['Rich',  40], ['Fred',  1000], ['Wilma',  0], ['',  56], ['Bam Bam', '']];
    foreach ($csv as $row) {
      $expect = array_shift($expectations);
      $this->assertEquals($expect[0], $row->Name);
      $this->assertEquals($expect[1], $row->Age);
      $i++;
    }
    $this->assertEquals(5, $i, "Expected 5 records, got $i");
  }


  public function testRandomAccess()
  {
    $csv = new CSVParser();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $this->assertEquals('Rich', $csv->Name);
    $this->assertEquals('Rich', $csv->getCell(0));
    $this->assertEquals('Rich', $csv->getCell(0,1));
    $this->assertEquals('Fred', $csv->getCell(0,2));
  }
  public function testMultiLineFromFile()
  {
    $csv = new CSVParser();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-2.csv');
    $i=0;
    $expectations = [['Fred',  "The Cave\nHovelshire"], ['Betty',  'Elsewhere']];
    foreach ($csv as $row) {
      $expect = array_shift($expectations);
      $this->assertEquals($expect[0], $row->Name);
      $this->assertEquals($expect[1], $row->Address);
      $i++;
    }
    $this->assertEquals(2, $i, "Expected 2 records, got $i");
  }
  public function testMultiLineFromString()
  {
    $csv = new CSVParser();
    $csv->loadFromString(file_get_contents(dirname(__FILE__) . '/fixtures/testcase-2.csv'));
    $i=0;
    $expectations = [['Fred',  "The Cave\nHovelshire"], ['Betty',  'Elsewhere']];
    foreach ($csv as $row) {
      $expect = array_shift($expectations);
      $this->assertEquals($expect[0], $row->Name);
      $this->assertEquals($expect[1], $row->Address);
      $i++;
    }
    $this->assertEquals(2, $i, "Expected 2 records, got $i");
  }

  /**
     * Test support for duplicate headers.
     */
  public function testDuplicateHeaders()
  {
    // Test *with* support for duplicate headers
    $csv = new CSVParser();
    $csv->supportDodgyHeaders();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-3-duplicate-headers.csv');
    $this->assertEquals(["Name", "Age", "Name_2", "Name_3"], $csv->getHeaders());
    $this->assertEquals(["Name", "Age", "Name", "Name"], $csv->getHeaders(FALSE));
    $this->assertEquals(1, $csv->Name);
    $this->assertEquals(2, $csv->Age);
    $this->assertEquals(3, $csv->Name_2);
    $this->assertEquals(4, $csv->Name_3);

    // Test *without* support for duplicate headers
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Duplicate header name: Name');
    $csv = new CSVParser();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-3-duplicate-headers.csv');
  }

  /**
     * Test support for empty headers.
     */
  public function testEmptyHeaders()
  {
    // Test *with* support for empty headers
    $csv = new CSVParser();
    $csv->supportDodgyHeaders();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-5-empty-headers.csv');
    $this->assertEquals(["missing_header_1", "a", "missing_header_2", "b", "missing_header_3"], $csv->getHeaders());
    $this->assertEquals(["", "a", "", "b", ""], $csv->getHeaders(FALSE));
    $this->assertEquals(1, $csv->missing_header_1);
    $this->assertEquals(2, $csv->a);
    $this->assertEquals(3, $csv->missing_header_2);
    $this->assertEquals(4, $csv->b);
    $this->assertEquals(5, $csv->missing_header_3);

    // Test *without* support for duplicate headers
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Missing header name for column 1');
    $csv = new CSVParser();
    $csv->loadFromFile(dirname(__FILE__) . '/fixtures/testcase-5-empty-headers.csv');
  }

  /**
     * Test remap
     */
  public function testRemap() {
    // First test we can do a 'normal' rename
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv->remapHeaders(['Name' => 'nombre', 'Age' => 'oldness']);
    $this->assertEquals(['nombre', 'oldness'], $csv->getHeaders());
    $this->assertEquals('Rich', $csv->nombre);
    $this->assertEquals(40, $csv->oldness);

    // First test we can rename the raw headers
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv->remapHeaders(['Name ' => 'nombre', '  Age' => 'oldness'], TRUE, FALSE);
    $this->assertEquals(['nombre', 'oldness'], $csv->getHeaders());
    $this->assertEquals('Rich', $csv->nombre);
    $this->assertEquals(40, $csv->oldness);

    // Finally test that we can toggle exceptions: first we ignore them.
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv->remapHeaders(['Name' => 'nombre', 'Hairstyle' => 'hair'], FALSE);
    $this->assertEquals(['nombre', 'Age'], $csv->getHeaders());
    $this->assertEquals('Rich', $csv->nombre);
    $this->assertEquals(40, $csv->Age);

    // Now we require them.
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Required header(s) missing: Hairstyle');
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv->remapHeaders(['Name' => 'nombre', 'Hairstyle' => 'hair']);
  }

  /**
   * test requiredHeaders method.
   */
  public function testRequired() {
    // First test we can do a 'normal' rename
    $csv = CSVParser::createFromFile(dirname(__FILE__) . '/fixtures/testcase-1.csv');
    $csv->requireHeaders(['Name', 'Age']);

    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage('Required header(s) missing: Hairstyle, Shoes');
    $csv->requireHeaders(['Name', 'Age', 'Hairstyle', 'Shoes']);
  }
}


